<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="./client/src/images/logo.png" width="60" />
  </a>
</p>
<h1 align="center">
  TechZone Coding Club Website
</h1>

- If you are wanting to work on this project then speak to Matt if you would like to contribute
- This project is using Gatsby with React.js

## Requirements

- **Node.js**
- **Node Package Manager (npm)**
- **Gatsby CLI**
  ```sh
  # Install Gatsby globally with -g flag
  npm install -g gatsby-cli
  ```

## Quick Start

1.  **Make sure you have the requirements listed above**

2.  **Clone the repo**

    ```sh
    # Clone repo
    git clone https://gitlab.com/MrCush/techzone-website.git
    ```

3.  **Install dependencies located that are listed in the `package.json` file**

    ```sh
    # Enter directory of repo and install dependencies
    cd techzone-website
    npm install
    ```

4.  **Start developing!**

    In order to start it up run:

    ```sh
    # In package.json you can see the different scripts
    npm run start
    ```

    Your live server is now running at:

    **`URL:`** http://localhost:8000

    **`GraphQL:`** http://localhost:8000/___graphql

- If you have any other questions, just ask Matt

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.
